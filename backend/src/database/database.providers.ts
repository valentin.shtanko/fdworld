import { createConnection } from 'typeorm';
import {User} from '../users/user.entity';

export const databaseProviders = [
    {
        provide: 'DATABASE_CONNECTION',
        useFactory: async () => await createConnection({
            type: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'fdwadmin',
            password: 'fdwpass',
            database: 'test',
            entities: [
               User,
            ],
            synchronize: true,
        }),
    },
];
