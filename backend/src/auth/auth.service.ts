import {Injectable, Logger} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import {ErrorField} from 'src/auth/models/error.model';
import {RegisterInputModel} from 'src/auth/models/register.model';
import {AccessToken} from 'src/auth/models/token.model';
import {User} from 'src/users/models/user.model';
import {UsersService} from '../users/users.service';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  constructor(
      private readonly usersService: UsersService,
      private readonly jwtService: JwtService,
  )
  {
  }

  async validateUser(username: string, pass: string): Promise<User | null> {
    const user = await this.usersService.findOne(username);
    if (user && bcrypt.compareSync(pass, user.password)) {
      const {password, ...result} = user;
      return result;
    }
    return null;
  }

  async login(user: any, ip): Promise<AccessToken> {
    const {username, password} = user;
    const authenticated = await this.validateUser(username, password);
    Logger.log(authenticated, 'Authenticated');
    if (!authenticated) {
      return {
        error:  'Неверный логин или пароль',
      }
    }
    if (authenticated) {
      const payload = {username: authenticated.username, userID: authenticated.id};
      Logger.log(payload, 'Payload');
      this.usersService.newIp(authenticated.id, ip);
      return {
        access_token: this.jwtService.sign(payload),
        user: authenticated
      };
    }

  }

  async register({email, username, password}: RegisterInputModel, ip): Promise<AccessToken> {
    let oldUser = await this.usersService.findByEmaail(email);
    let errors: ErrorField[] = [];

    if (oldUser) {
      errors.push({
        name: 'email',
        error: 'Данный email уже занят'
      })
    }
    oldUser = await this.usersService.findOne(username);
    if (oldUser) {
      errors.push({
        name: 'username',
        error: 'Логин уже используется'
      });
    }
    if (errors.length) {
      return {
        errorField: errors,
        error: ''
      };
    }

    const user = await this.usersService.create({email, username, password}, ip);
    const payload = {username: user.username, userId: user.id};
    return {
      access_token: this.jwtService.sign(payload),
      user: user
    }
  }

}
