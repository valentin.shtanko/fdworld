import {Field, ObjectType} from '@nestjs/graphql';

@ObjectType()
export class ErrorField {
    @Field()
    name?: string;
    @Field()
    error?: string
}
