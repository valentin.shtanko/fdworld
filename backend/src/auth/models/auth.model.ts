import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Auth {
    @Field()
    username: string;

    @Field()
    password: string;

}
