import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class RegisterInputModel {
    @Field()
    username: string;

    @Field()
    email: string;

    @Field()
    password: string;

}
