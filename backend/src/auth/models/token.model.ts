import {Field, ObjectType} from '@nestjs/graphql';
import {ErrorField} from 'src/auth/models/error.model';
import {User} from 'src/users/models/user.model';

@ObjectType()
export class AccessToken {
    @Field()
    access_token?: string;

    @Field(type => [ErrorField], { nullable: true })
    errorField?: ErrorField[];

    @Field({ nullable: true })
    error?: string;

    @Field(type => User, {nullable: true})
    user?: User;
}

