import {Ip, Logger, Req, Request, UseGuards} from '@nestjs/common';
import {Args, Mutation, Query, Resolver} from '@nestjs/graphql';
import {PubSub} from 'apollo-server-express';
import * as requestIp from 'request-ip';
import {UsersService} from '../users/users.service';
import {AuthService} from 'src/auth/auth.service';
import {GqlAuthGuard} from 'src/auth/guards/jwt-auth.guard';
import {AccessToken} from 'src/auth/models/token.model';
import {User} from 'src/users/models/user.model';
import {Auth} from './models/auth.model';

const pubSub = new PubSub();
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const CurrentUser = createParamDecorator(
    (data: unknown, context: ExecutionContext) => {
        const ctx = GqlExecutionContext.create(context);
        Logger.log(ctx.getContext().req.user);Ж:
        return ctx.getContext().req.user;
    },
);
export const IpAddress = createParamDecorator((data, req) => {
    const ctx = GqlExecutionContext.create(req);
    Logger.log( ctx.getContext().req.ip.match('\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b'), 'REMOTE');
    return ctx.getContext().req.ip.match('\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b')[0]; // In case we forgot
    // to include requestIp.mw() in main.ts
});
@Resolver(of => Auth)
export class AuthResolver {
    constructor(private readonly authService: AuthService, private readonly usersService: UsersService) {
    }


    @Mutation(returns => AccessToken)
    async login(@IpAddress() ip: string, @Args('username') username: string, @Args('password') password: string) {
        Logger.log(`${ip} ${username}:${password}`, 'Login');

        return this.authService.login({username, password}, ip);
    }
    @Mutation(returns => AccessToken)
    async register(@IpAddress() ip, @Args('username') username: string, @Args('email') email: string, @Args('password') password: string) {
        Logger.log(`${ip}|${username}:${email}:${password}`, 'Register');
        return this.authService.register({username, password, email}, ip);
    }

    @Query(returns => User)
    @UseGuards(GqlAuthGuard)
    whoAmI(@IpAddress() req, @CurrentUser() user: User) {
        Logger.log(req, 'WHO');
        return this.usersService.findOne(user.username);
    }
}
