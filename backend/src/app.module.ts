import {Module} from '@nestjs/common';
import {GraphQLModule} from '@nestjs/graphql';
import {TypeOrmModule} from '@nestjs/typeorm';
import {AuthModule} from 'src/auth/auth.module';
import {UsersModule} from 'src/users/users.module';

@Module({
    imports: [
        GraphQLModule.forRoot({
            context: ({ req }) => ({ req }),
            installSubscriptionHandlers: true,
            autoSchemaFile: 'schema.gql',
            playground: true
        }),
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'fdwadmin',
            password: 'fdwpass',
            database: 'test',
            entities: [''],
            synchronize: true,
        }),
        AuthModule,
        UsersModule
    ],
})
export class AppModule {
}
