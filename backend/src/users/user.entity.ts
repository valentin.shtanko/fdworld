
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({name: 'dle_users'})
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    email?: string;

    @Column()
    password?: string;

    @Column()
    register_ip?: string;

    @Column()
    logged_ip?: string;
}
