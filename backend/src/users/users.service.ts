import {Inject, Injectable, Logger} from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import {RegisterInputModel} from 'src/auth/models/register.model';
import {Repository} from 'typeorm';
import {User} from './user.entity';


@Injectable()
export class UsersService {
  private readonly users: User[];

  constructor(@Inject('USER_REPOSITORY')
  private usersRepository: Repository<User>,)
  {
  }

  async findOneById(id: number): Promise<User | undefined> {
    return this.usersRepository.findOne({
      where: {
        id,
      }
    });
    // return this.users.find(user => user.username === username);
  }

  async newIp(id, ip) {
    let user =  await this.usersRepository.findOne(id);
    user.logged_ip = ip;
    return this.usersRepository.save(user);
  }

  async findOne(username: string): Promise<User | undefined> {
    const user = this.usersRepository.findOne({
      where: {
        username,
      }
    });

    Logger.log(`${user}`, 'UserService|FindOne');
    return user;
    // return this.users.find(user => user.username === username);
  }

  async authenicate(username: string, password: string) {
    const user = this.usersRepository.findOne({
      where: {
        username: username,
      }
    });
    return user;
  }

  async findByEmaail(email: string): Promise<User | undefined> {
    return this.usersRepository.findOne({
      where: {
        email: email
      }
    })
  }

  async find(email: string, username: string): Promise<User[] | undefined> {
    return this.usersRepository.find({
      where: {
        email,
        username
      }
    })
  }

  async create({email, username, password}: RegisterInputModel, ip): Promise<User | undefined> {
    let user = this.usersRepository.create();
    user = {...user, email, username, password: bcrypt.hashSync(password, 10), register_ip: ip, logged_ip: ip};
    return this.usersRepository.save(user);
  }
}
