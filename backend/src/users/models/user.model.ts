import {Field, ID, ObjectType} from '@nestjs/graphql';

@ObjectType()
export class User {
    @Field(type => ID)
    id: number;

    @Field()
    username: string;

    @Field({nullable: true})
    email?: string;

    @Field({nullable: true})
    password?: string;
}
